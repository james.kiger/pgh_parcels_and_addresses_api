const parcelsModels = require('./parcels.models');

module.exports = {
        routes: (models) => {
          const controllers = require('./parcels.controllers')(models);
        return [{
          path: '/parcels/{addressToSearch}',
          method: 'GET',
          handler: controllers.getParcels,
          // TODO: Validation to prevent sql injection attack
          // TODO: Maybe add options here?
        }]
    }
}
