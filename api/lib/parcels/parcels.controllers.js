module.exports = (models) => {
    var parcel = models.Parcel
    return {
        getParcels: async function(request, h) {
            return parcel.getParcels(request.params.addressToSearch);
        }
    }
}
