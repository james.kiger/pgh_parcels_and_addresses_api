module.exports = {
    db: (sequelize, Sequelize) => {
      const { Model, QueryTypes } = require('sequelize');
      class Parcel extends Model {}

      Parcel.init({
        id: {
          type: Sequelize.INTEGER,
          field: 'ogc_fid',
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        pin: {
          type: Sequelize.STRING,
          field: 'pin'
        }, 
        geometry: {
          type: Sequelize.GEOMETRY,
          field: 'wkb_geometry',
        }
      }, {
        modelName: 'Parcel',
        tableName: 'allegheny_county_parcel_boundaries',
        sequelize,
        timestamps: false,  //excludes createdAt and updatedAt fields from query
      });

      Parcel.getParcels = async (address) => {
        const new_parcels = await sequelize.query(`SELECT allegheny_county__addressing_address_points.full_address AS full_address, allegheny_county_parcel_boundaries.pin AS parcel_id FROM allegheny_county_parcel_boundaries JOIN allegheny_county__addressing_address_points ON ST_Covers(allegheny_county_parcel_boundaries.wkb_geometry, allegheny_county__addressing_address_points.wkb_geometry) WHERE allegheny_county__addressing_address_points.full_address = '${address}';`, { type: QueryTypes.SELECT })

        return new_parcels;
      };

      return Parcel;
    }
  }
