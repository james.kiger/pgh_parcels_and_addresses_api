const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const Assert = require('assert');
const lab = exports.lab = Lab.script();
// const Lab = require('lab');
let server;
 
lab.before(async() => {
  const index = await require('../../index.js');
  server = await index.server;
  await index.sequelize;
});

lab.test('One Address may have multiple parcel ids - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
      method: 'GET',
      url: '/parcels/301 5TH AVE'
  });

  const payload = JSON.parse(response.payload);

  expect(payload).to.include({full_address:'301 5TH AVE',parcel_id:'0307E00069000000',full_address:'301 5TH AVE',parcel_id:'0001D00259000000',full_address:'301 5TH AVE',parcel_id:'0001D00259C71700',full_address:'301 5TH AVE',parcel_id:'0001D00259000A00',full_address:'301 5TH AVE',parcel_id:'0001D00259000C00'});
});

lab.test('One address that has one parcel id - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
      method: 'GET',
      url: '/parcels/221 PROVOST RD'
  });

  const payload = JSON.parse(response.payload);

  // TODO: make the matching less picky
  expect(payload).to.include({full_address: '221 PROVOST RD', parcel_id:'0138P00115000000'});
});
  
lab.test('An address may have a single parcel id - parcel.test.js', { timeout: 10000 }, async () => {
  // wait for the response and the request to finish
  const response = await server.inject({
      method: 'GET',
      url: '/parcels/0 EXAMPLE LANE'
  });

  const payload = JSON.parse(response.payload);

  //console.log(response)
  expect(payload).to.be.empty();
});

