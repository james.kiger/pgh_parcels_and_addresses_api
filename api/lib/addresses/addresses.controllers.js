module.exports = (models) => {
    var address = models.Address;
    return {
        getAddresses: async function(request, h) {
            return address.getAddresses(request.params.parcelToSearch);
        }
    }
}
