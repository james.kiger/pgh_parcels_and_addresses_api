module.exports = {
        routes: (models) => {
          const controllers = require('./addresses.controllers')(models);
        return [{
          path: '/addresses/{parcelToSearch}',
          method: 'GET',
          handler: controllers.getAddresses,
          options: {
          description: 'Get an array of addresses located within the specified parcel',
          notes: 'This currently assumes that parcel IDs are unique for all intents and purposes, or at least that if there is more than one object on the table sharing the identical parcel ID, it will also share the same geographic location. It also hardcodes the SRID. This might be potentially pulled out from the table instead. For more info on SRIDs see https://postgis.net/workshops/postgis-intro/projection.html',
          tags: ['api', 'Addresses', 'Parcels'],
        }}]
    }
}
