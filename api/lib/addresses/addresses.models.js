module.exports = {
    db: (sequelize, Sequelize) => {
      const { Model } = require('sequelize');
      class Address extends Model {}
      
      Address.init({
        id: {
          type: Sequelize.INTEGER,
          field: 'ogc_fid',
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },
        addr_num: {
          type: Sequelize.STRING,
          field: 'addr_num'
        },
        st_prefix: {
          type: Sequelize.STRING,
          field: 'st_prefix'
        },
        st_name: {
          type: Sequelize.STRING,
          field: 'st_name'
        },
        st_type: {
          type: Sequelize.STRING,
          field: 'st_type'
        },
        municipality: {
          type: Sequelize.STRING,
          field: 'municipality'
        },
        state: {
          type: Sequelize.STRING,
          field: 'state'
        },
        zip_code: {
          type: Sequelize.STRING,
          field: 'zip_code'
        },
        full_address: {
          type: Sequelize.STRING,
          field: 'full_address'
        }, 
      }, {
        modelName: 'Address',
        tableName: 'allegheny_county__addressing_address_points',
        sequelize,
        timestamps: false,  //excludes createdAt and updatedAt fields from query
      });

      Address.getAddresses = async (parcelID) => {
        const srid = 4326
        address_list = await sequelize.models.Parcel.findAll({
          attributes: {
              include: [[sequelize.fn('ST_AsText', sequelize.col('wkb_geometry')), 'geotext']]
          },
          where: {
              pin: parcelID
            }
          }).then(data => {
              return (
                  sequelize.models.Address.findAll({
                  where: sequelize.where(sequelize.fn('ST_CoveredBy', sequelize.col('wkb_geometry'), sequelize.fn('ST_GeomFromText', data[0].get('geotext'), srid)), 'TRUE'),
                  }).then(addresses => {
                      return addresses;
                  }));
          });
        return address_list;
      };

      return Address;
    }
}
