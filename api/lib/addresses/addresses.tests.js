const { expect } = require('@hapi/code');
const Lab = require('@hapi/lab');
const Assert = require('assert');
const lab = exports.lab = Lab.script();
let server;
 
lab.before(async() => {
    const index = await require('../../index.js');
    server = await index.server;
    await index.sequelize;
});

lab.test('A parcel id may have multiple Addresses - addresses.test.js', { timeout: 10000 }, async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
        method: 'GET',
        url: '/addresses/0098P00180000001'
    });

    const payload = JSON.parse(response.payload);

    // TODO: make the matching less picky
    // Right now if the following only has 311 WASHINGTON RD the test will fail
    expect(payload).to.include({id: 500690,
        addr_num: '311',
        st_prefix: null,
        st_name: 'WASHINGTON',
        st_type: 'RD',
        municipality: 'MT LEBANON',
        state: 'PA',
        zip_code: '15216',
        full_address: '311 WASHINGTON RD',
        id: 500696,
        addr_num: '315',
        st_prefix: null,
        st_name: 'WASHINGTON',
        st_type: 'RD',
        municipality: 'MT LEBANON',
        state: 'PA',
        zip_code: '15216',
        full_address: '315 WASHINGTON RD',
        id: 500697,
        addr_num: '401',
        st_prefix: null,
        st_name: 'WASHINGTON',
        st_type: 'RD',
        municipality: 'MT LEBANON',
        state: 'PA',
        zip_code: '15216',
        full_address: '401 WASHINGTON RD'
    });
});

lab.test('A parcel id may have no addresses - addresses.test.js', { timeout: 10000 }, async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
        method: 'GET',
        url: '/addresses/0244K00100000000'
    });
    const payload = JSON.parse(response.payload);

    expect(payload).to.be.empty();
});

lab.test('A parcel id may have a single address - addresses.test.js', { timeout: 10000 }, async () => {
    // wait for the response and the request to finish
    const response = await server.inject({
        method: 'GET',
        url: '/addresses/0014F00218000000'
    });

    const payload = JSON.parse(response.payload);

    //console.log(response)
    expect(payload).to.include({id: 112604,
        addr_num: '843',
        st_prefix: null,
        st_name: 'CLIMAX',
        st_type: 'ST',
        municipality: 'PITTSBURGH',
        state: 'PA',
        zip_code: '15210',
        full_address: '843 CLIMAX ST'});
});
