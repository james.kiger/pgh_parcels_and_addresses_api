#!/bin/sh

# Download, run ogr2ogr, move to postgis/imports directory
cd /home/gdal/downloads

get_addresses(){
  echo "Downloading addresses..."
  wget -q -O addresses.geojson https://data.wprdc.org/dataset/4988ae5c-a677-4a7f-9bd0-e735c19a8ff3/resource/492a62fb-0560-43f0-9f8e-037fc51343a0/download/allegheny_county__addressing_address_points.geojson

  echo "Converting addresses..."
  ogr2ogr --config PG_USE_COPY YES -f PGDump addresses.sql addresses.geojson

  echo "Moving addresses..."
  mv addresses.sql /home/postgis/imports

  echo "Cleaning up addresses..."
  rm addresses.geojson

  echo "Done with addresses."
}


get_parcels() {
  echo "Downloading parcels..."
  wget -q -O parcels.geojson https://data.wprdc.org/dataset/709e4e52-6f82-4cd0-a848-f3e2b3f5d22b/resource/3f50d47a-ab54-4da2-9f03-8519006e9fc9/download/allegheny_county_parcel_boundaries.geojson

  echo "Converting parcels..."
  ogr2ogr --config PG_USE_COPY YES -f PGDump parcels.sql parcels.geojson

  echo "Moving parcels..."
  mv parcels.sql /home/postgis/imports

  echo "Cleaning up parcels..."
  rm parcels.geojson

  echo "Done with parcels."
}

get_neighborhoods(){
  echo "Downloading neighborhoods..."
  wget -q -O neighborhoods.geojson https://pghgishub-pittsburghpa.opendata.arcgis.com/datasets/dbd133a206cc4a3aa915cb28baa60fd4_0.geojson

  echo "Renaming neighborhoods..."
  sed -i -e "s|Neighborhoods_|Pittsburgh_Neighborhoods|" neighborhoods.geojson

  echo "Converting neighborhoods..."
  ogr2ogr --config PG_USE_COPY YES -f PGDump neighborhoods.sql neighborhoods.geojson

  echo "Moving neighborhoods..."
  mv neighborhoods.sql /home/postgis/imports

  echo "Cleaning up neighborhoods..."
  rm neighborhoods.geojson

  echo "Done with neighborhoods."
}

get_addresses &
get_parcels &
get_neighborhoods &
wait 

echo "Done with all data sets."
exit 0